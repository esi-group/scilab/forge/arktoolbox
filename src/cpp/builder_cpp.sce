// This file is released under the 3-clause BSD license. See COPYING-BSD.

// This macro compiles the files

function builder_cpp()
  src_cpp_path = get_absolute_file_path("builder_cpp.sce");


  CFLAGS = ilib_include_flag(src_cpp_path);
  LDFLAGS = ""

  CFLAGS = CFLAGS + " -g" // debugging

  if (getos()=="Darwin") then
    CFLAGS = CFLAGS + " -I/opt/local/include";
    LDFLAGS = LDFLAGS + " /opt/local/lib/libboost_thread-mt.a"
    LDFLAGS = LDFLAGS + " /opt/local/lib/libboost_system-mt.a"
    LDFLAGS = LDFLAGS + " /opt/local/lib/libplibjs.a"
    LDFLAGS = LDFLAGS + " /opt/local/lib/libplibul.a"
  elseif (getos()=="Linux") then
    LDFLAGS = LDFLAGS + " /usr/local/lib/libboost_thread-mt.a"
    LDFLAGS = LDFLAGS + " /usr/local/lib/libboost_system-mt.a"
    LDFLAGS = LDFLAGS + " /usr/local/lib/libplibjs.a"
    LDFLAGS = LDFLAGS + " /usr/local/lib/libplibul.a"
  elseif (getos()<>"Windows") then
    LDFLAGS = LDFLAGS + " /usr/local/lib/libboost_thread-mt.a"
    LDFLAGS = LDFLAGS + " /usr/local/lib/libboost_system-mt.a"
    LDFLAGS = LDFLAGS + " /usr/local/lib/libplibjs.a"
    LDFLAGS = LDFLAGS + " /usr/local/lib/libplibul.a"
  end

  if (getos()<>"Windows") then
    if ~isdir(SCI+"/../../share") then
      // Source version
      CFLAGS = CFLAGS + " -I" + SCI + "/modules/scicos_blocks/includes" ;
      CFLAGS = CFLAGS + " -I" + SCI + "/modules/scicos/includes" ;
    else
      // Release version
      CFLAGS = CFLAGS + " -I" + SCI + "/../../include/scilab/scicos_blocks";
      CFLAGS = CFLAGS + " -I" + SCI + "/../../include/scilab/scicos";
    end
  else
    CFLAGS = CFLAGS + " -I" + SCI + "/modules/scicos_blocks/includes";
    CFLAGS = CFLAGS + " -I" + SCI + "/modules/scicos/includes";
    // Getting symbols
    if findmsvccompiler() <> "unknown" & haveacompiler() then
      LDFLAGS = LDFLAGS + " """ + SCI + "/bin/scicos.lib""";
      LDFLAGS = LDFLAGS + " """ + SCI + "/bin/scicos_f.lib""";
    end
  end

  //ilib_verbose(2);

  entry_points = [..
    "block_joystick", ..
    "block_mavlink", ..
    ];
   
  srcs =  [..
    "block_joystick.cpp", ..
    "block_mavlink.cpp", ..
    "AsyncSerial.cpp",..
    "Joystick.cpp",..
    "MAVLinkParser.cpp",..
    "utilities.cpp",..
    ];
  
  tbx_build_src(entry_points, srcs, ..
                "c",                                  ..
                src_cpp_path,                         ..
                [],                                   ..
                LDFLAGS,                              ..
                CFLAGS,                               ..
                "",                                   ..
                "",                                   ..
                "arktoolbox_cpp");
endfunction

builder_cpp();
clear builder_cpp; // remove builder_cpp on stack
